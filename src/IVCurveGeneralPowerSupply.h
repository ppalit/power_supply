/*!
 * \authors Pritam Palit <ppalit@cern.ch>, SINP, Kolkata
 * \date July 24 2020
 */

#ifndef IVCurveGeneralPowerSupply_H
#define IVCurveGeneralPowerSupply_H

#include <iomanip>
#include <iostream>
#include "PowerSupply.h"
#include "PowerSupplyChannel.h"
#include "DeviceHandler.h"
#include "Keithley.h"
#include <vector>


/*! 

------------------------------------
\class IVCurveGeneralPowerSupply
\brief IV curve for any general power supply

*/

class IVCurveGeneralPowerSupply
{
    public : 
        IVCurveGeneralPowerSupply(DeviceHandler& thehandler, std::string powersupplyName, std::vector<std::string> channelNameList);

        virtual ~IVCurveGeneralPowerSupply();
        virtual void configure(); // Starts configuring the config file
        virtual void startReading(); // Ramps up the voltage, scans the IV curve
        virtual void stopReading();  // Make the output of IV curve, Ramps down the voltage
        void WriteIVAfterRamp(PowerSupplyChannel* channel, float rampvoltage);
        void scanIVKeithley(pugi::xml_document& docSettings, std::string channelOption, std::vector<std::string> outputNameExtension);
        void scanIVCaen(pugi::xml_document& docSettings, std::string channelOption, std::vector<std::string> outputNameExtension);
        void plotIV(std::string outputfolder, int npoints);
        void plotIVAverage(std::string outputfolder, std::vector<std::string> outputNameExtension);
        int setPolarity(std::string pol_string);
        float getVRangeFromVmax(int vMax);
        void preliminaryPSSettings(KeithleyChannel* channel);
        void switchPSOff(PowerSupplyChannel* psChannel, float voltStep, float timeStep);
        //void switchPSOffCaen(std::vector<CAENChannel*> psChannel, float voltStep, float timeStep);
        void printPresentVoltageAndCurrent(PowerSupplyChannel* psChannel);
        //void printPresentVoltageAndCurrentCaen(CAENChannel* psChannel);
        void wait();
        std::vector<float> rampvoltagevaluesvector(std::string str);
    
    private:
        PowerSupply* fPowersupply;
        std::vector<PowerSupplyChannel*> fChannelList; //=  new std::vector<PowerSupplyChannel*>();
        DeviceHandler fThehandler;
        std::string fPowerSupplyName;
        std::vector<std::string> fChannelNameList;
        std::vector<std::string> fCsvfileName;
        int fNPoints;
        //boost::program_options::variables_map vmap;
        std::string docPath;
};


#endif
